# -*- coding: utf-8 -*-
from selenium import webdriver
import pymongo
from pymongo import MongoClient
import datetime
from dateutil import parser
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from multiprocessing import Process
import sys
import argparse

parser1 = argparse.ArgumentParser()
parser1.add_argument('--verb', action='store_true',default=False)
args = parser1.parse_args()


repeated_comment = 0


"""
	connection_params = {
		'host': 'localhost',
		'namespace': 'reclamos',

	}
	connection = MongoClient(
		'mongodb://{host}/{namespace}'.format(**connection_params)
	)

"""



def connect():
	connection_params = {
		'user': "anastasia",
		'password': 'mongo123456',
		'host': 'localhost',
		'namespace': 'reclamos',
	}
	connection = MongoClient('mongodb://{user}:{password}@{host}/{namespace}'.format(**connection_params))




	db = connection.reclamos

	if args.verb:
		print(db.collection_names())
	return db


def get_comment_data(driver,db):
	content = {}
	try:
		element = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, ".//span[@property='v:description']/p")))
		element = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, ".//span[@property='v:dtreviewed']")))
		element = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, ".//span[@property='v:summary']")))
		content["text"] = driver.find_element_by_xpath(".//span[@property='v:description']/p").text
		date = driver.find_element_by_xpath(".//span[@property='v:dtreviewed']").text
		original_date = date
		date = date.replace(',','')
		date = date.strip().split()

		if date[2]=='Agosto':
			date[2]= 'August'
		if date[2]=='Enero':
			date[2]='January'
		if date[2]=='Abril':
			date[2]='April'
	
		try:
			date_format = parser.parse("{} {} {}".format(date[2][0:3], date[1], date[-1]))
		except:
			if args.verb:
				print("Bad date format")
			return {}

		content["comment-date"] = date_format
		content["title"] = driver.find_element_by_xpath(".//span[@property='v:summary']").text
		posts = db.posts
		other_equals = posts.find({'comment': content}).count()
		if other_equals>0:
			global repeated_comment
			repeated_comment+=1
			content={}
	except:
		if args.verb:
			print("get_comment Error")
		content={}
	return content


def next_page(site,name,driver,db):
	try:

		container = driver.find_element_by_xpath("//span[@class = 'pager-list']")
		next_active = container.find_element_by_xpath(".//a[@class = 'pager-next active']")
		next_active.click()
		driver.implicitly_wait(30)
		return True
	except:
		if args.verb:
			print("No more pages")
		driver.back()
		driver.implicitly_wait(30)
		return False


def get_list_comment(site,name, index,driver,db):
	#try:
	row = driver.find_elements_by_xpath(".//td/a")
	global repeated_comment
	if repeated_comment > 3:
		repeated_comment=0
		return False

	if index >= len(row):
		return False
		"""
		if next_page(site,name,driver,db):
			index = 0
			return get_list_comment(site,name, index,driver,db)
		else:
			return False
		"""

	row[index].click()
	driver.implicitly_wait(30)

	comment = get_comment_data(driver,db)

	if len(comment)>0:
		post = {"category": site, "institution": name, "comment": comment, "date": datetime.datetime.utcnow()}
		posts = db.posts
		posts.insert_one(post)

	driver.back()
	driver.implicitly_wait(30)
	if args.verb:
		print(index)
	return get_list_comment(site,name, index+1,driver,db)
	#except:
		#print("No more get_list_comment")
		#return False


def get_company_claims(site,index,url,driver,db):
	try:
		if args.verb:
			print(url)
		driver.implicitly_wait(30)
		driver.get(url)
		element = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.ID,"block-elementos-0")))
		elements = driver.find_element_by_id("block-elementos-0");
		get_div = elements.find_element_by_class_name("content")
		get_li = get_div.find_elements_by_xpath(".//ul/li/a")

	except:
		if args.verb:
			print("Fail to load company_claims block")
		return False

	if index >= len(get_li):
		return False
	if args.verb:
		print(get_li[index].text)

	if get_li[index].text[0]!="-":
		name = get_li[index].text
		get_li[index].click()
		driver.implicitly_wait(30)
		number = -1
		try:
			container = driver.find_element_by_xpath("//div[@class = 'pager']")
			next_active = container.find_element_by_xpath(".//a[@class = 'pager-last active']")
			next_active.click()
			driver.implicitly_wait(30)
			container = driver.find_element_by_xpath("//div[@class = 'pager']")
			current_page = container.find_element_by_xpath(".//strong[@class = 'pager-current']")
			try:
				number = current_page.text
				number = int(number)
			except:
				if args.verb:
					print("No se puede parsear numero")
			first_active = next_active = container.find_element_by_xpath(".//a[@class = 'pager-first active']")
			first_active.click()

			driver.implicitly_wait(30)
		except:
			pass
		if args.verb:
			print(number)
		if number!= -1:
			for i in range(number):
				if args.verb:
					print("pagina",i)
				get_list_comment(site,name, 0,driver,db)
				next_page(site,name,driver,db)
		else:
			get_list_comment(site,name, 0,driver,db)

			#continue





def get_site(site):
		db = connect()
		url = "https://www.reclamos.cl/{}".format(site)
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-impl-side-painting')
		chrome_options.add_argument('--disable-gpu-sandbox')
		chrome_options.add_argument('--disable-accelerated-2d-canvas')
		chrome_options.add_argument('--disable-accelerated-jpeg-decoding')
		chrome_options.add_argument('--test-type=ui')
		driver = webdriver.Chrome(r"../chromedriver", chrome_options=chrome_options)
		driver.implicitly_wait(30)

		try:
			driver.implicitly_wait(30)
			driver.get(url)
			element = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.ID,"block-elementos-0")))
			elements = driver.find_element_by_id("block-elementos-0");
			get_div = elements.find_element_by_class_name("content")
			get_li = get_div.find_elements_by_xpath(".//ul/li/a")

		except:
			if args.verb:
				print("Fail to load company_claims block")
			return False
		for i in range(len(get_li)):
			try:
				get_company_claims(site,i,url,driver,db)
			except:
				if args.verb:
					print('Error getting claims from: ',site,i)
				pass
		return





if __name__ == '__main__':
	#Url del sitio
	sites = ['salud','telecomunicaciones','bancos','educacion','gobierno','servicios_basicos','automotriz','construccion','transportes','retail']
	sites.reverse()
	proc = []
	for site in sites:
	   	proc.append(Process(target=get_site, args=(site,)))
	for p in proc:
	    p.start()
	    p.join()

		#except:
			#print("Bad url:",url)
